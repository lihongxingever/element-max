import consola from 'consola'
import chalk from 'chalk'

import fs from 'fs'
import path from 'path'
import prompt from 'prompt'

import { fileURLToPath } from 'url'
const __filenameNew = fileURLToPath(import.meta.url)
console.log(import.meta, 'import.meta')
const __dirnameNew = path.dirname(__filenameNew)

async function updateVersion() {
  const pkgCtx = await fs.readFileSync(path.resolve(__dirnameNew, '../package.json')).toString()
  const pkgCtxParse = JSON.parse(pkgCtx)
  const version = pkgCtxParse.version
  console.log('version', version)
  consola.log(chalk.cyan('Start updating version'))
  consola.log(chalk.cyan(`$CURRENT_VERSION: ${version}`))
  prompt.get([{name: 'version', required: true}], async (err, res) =>{
    console.log(err, res)
    if (!err) {
      const newCtx = { ...pkgCtxParse,  ...res}
      await fs.writeFileSync(path.resolve(__dirnameNew, '../package.json'), JSON.stringify(newCtx))
    }
  })
}

updateVersion()
