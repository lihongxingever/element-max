import { copy } from 'fs-extra'
export default function copyFile() {
  return {
    name: 'copy-assets-plugin',
    buildEnd() {
      // 复制文件或文件夹
      copy('theme-chalk', 'dist/theme-chalk', { overwrite: true });
    }
  }
}
