import { defineConfig } from 'vite'
import { resolve } from 'path'
import vue from '@vitejs/plugin-vue'
import copyFile from './vite-plugin-copy'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), copyFile()],
  build: {
    lib: {
      entry: resolve(__dirname, 'lib/main.js'),
      name: 'element-mixc',
      fileName: 'index'
    },
    rollupOptions: {
      // 确保外部化处理那些你不想打包进库的依赖
      external: ['vue']
    },
    onEnd() {
      console.log('onEnd');
      copy('theme-chalk', 'dist/theme-chalk')
    }
  }
})
