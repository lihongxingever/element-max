import { ref, watch } from 'vue';

export function usePagination(listInit, pagesize = 20) {
  const page_num = ref(1)
  const page_size = ref(pagesize)
  watch([page_num, page_size], () => {
    listInit()
  })
  function setPageNum() {
    page_num.value = 1
  }
  return {
    page_num,
    page_size,
    setPageNum
  }
}
