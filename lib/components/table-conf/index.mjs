import { withInstall } from '../../utils/vue/install.mjs';
import tableConf from './index.vue'

const MIXTableConfig = withInstall(tableConf);

export {MIXTableConfig, MIXTableConfig as default}
