export { ElTable, ElTableColumn } from './components/table/index.mjs';

function install(app) {
  const components = [ElTable, ElTableColumn]
  components.forEach((item) => {
    if (item.install) {
      app.use(item);
    } else if (item.name) {
      app.component(item.name, item);
    }
  });
}

const defa = {
  install
}

export * from './components/table/index.mjs'
export * from './components/table-conf/index.mjs'
export * from './components/loading/index.mjs'
export { vLoading as ElLoadingDirective, vLoading } from './components/loading/src/directive.mjs';

export { defa as default, install }
